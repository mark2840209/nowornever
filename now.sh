#!/bin/bash

JANELA_BOAS_VINDAS=$(yad --center --title="Meu Script" \
                      --width=400 --height=400 \
                      --text="<span font_desc='20' weight='bold' underline='single'>Bem-vindo !\n\n → Você fala com o grande REI PENGUIM.\n → Aperte em iniciar !!!</span>" --text-align="center" \
                      --button="Iniciar:0" --image="$CAMINHO_IMAGEM" --html)

ESCOLHA_LOGIN=$(yad --center --title=" SCP script " \
                  --width=400 --height=50 \
                  --text="Você Deseja fazer um login existente ou um novo login?" --text-align="center" \
                  --button="Novo Login":0 --button="Login Existente":1)

OPCAO=$?

if [ $OPCAO -eq 1 ]; then
    LOGIN_ANTERIOR=$(yad --center --title="Meu Script" \
                          --width=400 --height=400 \
                          --text="Ultimos logins: " \
                          --form \
                          --field=":CB" "$(cat historico-acessos.txt | tr "\n" "user@ip ")"
                      )

    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        exit 1
    fi

    LOGIN_INFO=$(echo "$LOGIN_ANTERIOR" | cut -d "@" -f 1)
    IP_USUARIO=$(echo "$LOGIN_ANTERIOR" | cut -d "@" -f 2 | tr -d "|")

    SENHA_DIGITADA=$(yad --form --field="Digite a senha: ":H)

    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        exit 1
    fi

    SENHA=$(echo $SENHA_DIGITADA | cut -d "|" -f 1)

elif [ $OPCAO -eq 0 ]; then
    LOGIN_NOVO=$(yad --center --title="Meu Script" \
                      --width=400 --height=400 \
                      --text="Digite o nome de usuário e o IP da máquina remota a ser feita a operação no formato user@ip" --text-align="center" \
                      --form \
                      --field="Usuário@IP:  " "" \
                      --field="Senha:  ":H)

    INFO_LOGIN=$(echo "$LOGIN_NOVO" | cut -d "|" -f 1)
    IP_USUARIO=$(echo "$INFO_LOGIN" | cut -d "@" -f 2)
    SENHA_DIGITADA=$(echo "$LOGIN_NOVO" | cut -d "|" -f 2)

    SENHA=$(echo "$SENHA_DIGITADA" | cut -d "|" -f 1)

    echo "$INFO_LOGIN" >> historico-acessos.txt

    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        exit 1
    fi

else
    exit 1
fi

MINICIAL=$(yad --center --title="Meu Script" \
               --width=400 --height=50 \
               --text="Bem vindo ao Meu Script!" --text-align="center" \
               --form \
               --field="Escolha a operação desejada: ":CB "Upload!Download")

EXIT_CODE=$?
if [ $EXIT_CODE -ne 0 ]; then
    exit 1
fi

OPCAO=$(echo "$MINICIAL" | cut -d "|" -f 1)

if [ "$OPCAO" == "Upload" ]; then
    ARQUIVOS=$(yad --center --title="Meu Script" \
                  --width=400 --height=400 \
                  --text="Escolha o arquivo para fazer upload" --text-align="center" \
                  --file --multiple)

    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        exit 1
    fi

    ARQUIVO=$(echo "$ARQUIVOS" | tr -s "|" " ")

    if [ $? -eq 0 ]; then
        DIRETORIO_REMOTO="/"
    else
        exit 1
    fi

    while true; do
        sshpass -p $SENHA ssh $INFO_LOGIN "ls -l -p -a '$DIRETORIO_REMOTO' | grep '^d' | awk '{print \$NF}'" > /tmp/diretorios_remotos.txt
        DIRETORIO_SELECIONADO=$(yad --list --title="$INFO_LOGIN" --dual --text-align="center" --text="Diretório: $DIRETORIO_REMOTO" --column="Diretórios" --separator='\n' --width=400 --height=400 --print-column=1 < /tmp/diretorios_remotos.txt)

        EXIT_CODE=$?
        if [ $EXIT_CODE -ne 0 ]; then
            exit 1
        fi

        if [ $? -eq 0 ] && [ -n "$DIRETORIO_SELECIONADO" ]; then
            DIRETORIO_REMOTO="$DIRETORIO_REMOTO$DIRETORIO_SELECIONADO"
        else
            echo "" > /tmp/diretorios-remotos.txt
            break
        fi
    done

    sshpass -p $SENHA scp $ARQUIVO $INFO_LOGIN:$DIRETORIO_REMOTO
fi

if [ "$OPCAO" == "Download" ]; then
    DIRETORIO_DESTINO=$(yad --center --title="Meu Script" \
                           --width=400 --height=400 \
                           --text="Escolha o diretório para receber o arquivo" --text-align="center" \
                           --file --directory)

    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        exit 1
    fi

    if [ $? -eq 0 ]; then
        DIRETORIO_REMOTO="/"
    else
        exit 1
    fi

    while true; do
        sshpass -p $SENHA ssh $INFO_LOGIN "ls -p '$DIRETORIO_REMOTO' | tr ' ' '\n' " > /tmp/arquivos_remotos.txt
        ARQUIVO_SELECIONADO=$(yad --list --title="$INFO_LOGIN" --text="Diretório: $DIRETORIO_REMOTO" --text-align="center" --column="Arquivos" --separator='\n' --width=400 --height=400 --print-column=1 < /tmp/arquivos_remotos.txt)

        EXIT_CODE=$?
        if [ $EXIT_CODE -ne 0 ]; then
            exit 1
        fi

        if [ $? -eq 0 ] && [ -n "$ARQUIVO_SELECIONADO" ] && [[ $ARQUIVO_SELECIONADO =~ /$ ]]; then
            DIRETORIO_REMOTO="$DIRETORIO_REMOTO$ARQUIVO_SELECIONADO"
        else
            DIRETORIO_REMOTO="$DIRETORIO_REMOTO$ARQUIVO_SELECIONADO"
            echo "" > /tmp/arquivos-remotos.txt
            break
        fi
        EXIT_CODE=$?
    done

    DIRETORIO_DESTINO=$(echo "$DIRETORIO_DESTINO" | cut -d "|" -f 1)

    sshpass -p $SENHA scp $INFO_LOGIN:$DIRETORIO_REMOTO $DIRETORIO_DESTINO
fi
